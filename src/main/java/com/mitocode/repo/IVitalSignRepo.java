package com.mitocode.repo;

import com.mitocode.model.VitalSign;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface IVitalSignRepo extends IGenericRepo<VitalSign, Integer> {
    @Query("FROM VitalSign c WHERE c.patient.dni = :dni OR LOWER(c.patient.firstName) LIKE %:fullname% OR LOWER(c.patient.lastName) LIKE %:fullname%") //JPQL
    List<VitalSign> search(@Param("dni") String dni, @Param("fullname") String fullname);

}
