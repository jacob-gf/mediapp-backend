package com.mitocode.controller;

import com.mitocode.dto.*;
import com.mitocode.model.VitalSign;
import com.mitocode.service.IVitalSignService;
import com.mitocode.service.IMediaFileService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/vital-sign")
@RequiredArgsConstructor
public class VitalSignController {

    //@Autowired
    private final IVitalSignService service;


    @Qualifier("consultMapper")
    private final ModelMapper mapper;

    @GetMapping
    public ResponseEntity<List<VitalSignDTO>> findAll(){
        List<VitalSignDTO> list = service.findAll().stream().map(this::convertToDto).collect(Collectors.toList());
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<VitalSignDTO> findById(@PathVariable("id") Integer id){
        VitalSignDTO dto = this.convertToDto(service.findById(id));
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Void> save(@Valid @RequestBody VitalSignDTO request){
        VitalSign cons = this.convertToEntity(request);

        VitalSign obj = service.saveTransactional(cons);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdVitalSign()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<VitalSignDTO> update(@Valid @PathVariable("id") Integer id, @RequestBody VitalSignDTO dto){
        dto.setIdVitalSign(id);
        VitalSign obj = service.update(this.convertToEntity(dto), id);
        return new ResponseEntity<>(this.convertToDto(obj), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/search/others")
    public ResponseEntity<List<VitalSignDTO>> searchByOthers(@RequestBody FilterVitalSignDTO filterDTO){
        List<VitalSign> consults = service.search(filterDTO.getDni(), filterDTO.getFullname());
        List<VitalSignDTO> consultDTOS = mapper.map(consults, new TypeToken<List<VitalSignDTO>>(){}.getType());

        return new ResponseEntity<>(consultDTOS, HttpStatus.OK);
    }

    private VitalSignDTO convertToDto(VitalSign obj){
        return mapper.map(obj, VitalSignDTO.class);
    }

    private VitalSign convertToEntity(VitalSignDTO dto){
        return mapper.map(dto, VitalSign.class);
    }
}
