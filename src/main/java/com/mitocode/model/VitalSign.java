package com.mitocode.model;

import com.mitocode.dto.IConsultProcDTO;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class VitalSign {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Integer idVitalSign;

    @ManyToOne //FK
    @JoinColumn(name = "id_patient", nullable = false, foreignKey = @ForeignKey(name = "FK_VITAL_SIGN_PATIENT"))
    private Patient patient; //JPQL Java Persistence Query Language //FROM Consult c WHERE c.patient.dni = ?


    @Column(nullable = false)
    private String temperature;

    @Column(nullable = false)
    private String pulse;

    @Column(nullable = false)
    private String heartbeat;

    @Column(nullable = false)
    private LocalDateTime date;

}
