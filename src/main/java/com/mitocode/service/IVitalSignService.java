package com.mitocode.service;

//import com.mitocode.dto.VitalSignProcDTO;
//import com.mitocode.dto.IVitalSignProcDTO;
import com.mitocode.dto.VitalSignDTO;
import com.mitocode.model.VitalSign;
import com.mitocode.model.Exam;

import java.time.LocalDateTime;
import java.util.List;

public interface IVitalSignService extends ICRUD<VitalSign, Integer>{

    VitalSign saveTransactional(VitalSign consult);
    List<VitalSign> search(String dni, String fullname);

}
