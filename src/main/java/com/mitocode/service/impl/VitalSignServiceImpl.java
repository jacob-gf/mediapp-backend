package com.mitocode.service.impl;

//import com.mitocode.dto.VitalSignProcDTO;
//import com.mitocode.dto.IVitalSignProcDTO;
import com.mitocode.dto.VitalSignDTO;
import com.mitocode.model.VitalSign;
import com.mitocode.model.Exam;
//import com.mitocode.repo.IVitalSignExamRepo;
import com.mitocode.repo.IVitalSignRepo;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.service.IVitalSignService;
import lombok.RequiredArgsConstructor;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class VitalSignServiceImpl extends CRUDImpl<VitalSign, Integer> implements IVitalSignService {

    //@Autowired
    private final IVitalSignRepo consultRepo;


    @Override
    protected IGenericRepo<VitalSign, Integer> getRepo() {
        return consultRepo;
    }

    @Transactional //(propagation = Propagation.REQUIRES_NEW)
    @Override
    public VitalSign saveTransactional(VitalSign request) {
        consultRepo.save(request);
        return request;
    }

    @Override
    public List<VitalSign> search(String dni, String fullname) {
        return consultRepo.search(dni, fullname);
    }

}
